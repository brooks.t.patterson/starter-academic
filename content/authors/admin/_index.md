---
# Display name
title: Brooks Patterson

# Full name (for SEO)
first_name: Brooks
last_name: Patterson

# Status emoji
status:
  icon: 

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: 

# Organizations/Affiliations to show in About widget
organizations:

# Short bio (displayed in user profile at end of posts)
bio: 

# Interests to show in About widget
interests:

# Education to show in About widget
education:

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/brookstpat
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/brookstpatterson/
- icon: instagram
  icon_pack: fab
  link: https://www.instagram.com/brookstpatterson/
- icon: strava
  icon_pack: fab
  link: https://www.strava.com/athletes/brooks_patterson
  
  # Link to a PDF of your resume/CV.
  # To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  # icon: cv
  # icon_pack: ai
  # link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false
---