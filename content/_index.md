---
# Leave the homepage title empty to use the site title
title: Pedal Forward
date: 2023-2-23
type: landing

sections:
  - block: markdown
    id: Homepage Hero
    content:
      title: Just Keep **Pedaling**
    design:
      background:
        image: 
          # Name of image in `assets/media/`.
          filename: pisgahmud.jpg
          # Apply image filters?
          filters:
            # Darken the image? Range 0-1 where 1 is transparent and 0 is opaque.
            brightness:
          #  Image fit. Options are `cover` (default), `contain`, or `actual` size.
          size: actual
          # Image focal point. Options include `left`, `center` (default), or `right`.
          position: center
          # Use a fun parallax-like fixed background effect on desktop? true/false
          parallax: false
          # Text color (true=light, false=dark, or remove for the dynamic theme color).
          text_color_light: true
      spacing:
        padding: ["200px", "0", "200px", "0"]
  - block: markdown
    id: Homepage description 
    content:
      title: Why Pedal Forward?
      subtitle: For Encouragement & Progress
      text: >-

        Because writing helps me get unstuck. It helps me make progress, and progress is important. But progress is hard, and we could all use more encouragement. This site is a repository of writing to make progress. My hope is that by following along you might learn something, or find some encouragement, that helps you make progress too. 

        The pedal piece? I just love bikes, and I think [the world would be a better place](https://www.sciencedirect.com/science/article/pii/S016041201831314X?via%3Dihub) if we all rode bikes more often. 
    design:
      columns: "1"
      spacing:
        padding: ["20px", "0", "20px", "0"]
  - block: portfolio
    id: category filter
    content:
      title: Latest Writing
      filters:
        folders:
          - post
      # Default filter index (e.g. 0 corresponds to the first `filter_button` instance below).
      default_button_index: 0
      # Filter toolbar (optional).
      # Add or remove as many filters (`filter_button` instances) as you like.
      # To show all items, set `tag` to "*".
      # To filter by a specific tag, set `tag` to an existing tag name.
      # To remove the toolbar, delete the entire `filter_button` block.
      buttons:
        - name: All
          tag: '*'
        - name: Faith
          tag: faith
        - name: Work
          tag: work
        - name: Learning
          tag: learning
    design:
    # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '2'
      view: list
      # For Showcase view, flip alternate rows?
      flip_alt_rows: false
  - block: about.avatar
    id: about
    content:
      # Choose a user profile to display (a folder name within `content/authors/`)
      username: admin
      # Override your bio text from `authors/admin/_index.md`?
      text:
  - block: contact
    id: contact
    content:
      title: Connect
      # Automatically link email and phone or display as text?
      autolink: false
      # Email form provider
      form:
        provider: netlify
        formspree:
          id:
        netlify:
          # Enable CAPTCHA challenge to reduce spam?
          captcha: false
    design:
      columns: '2'
---