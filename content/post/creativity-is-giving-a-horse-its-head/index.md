---
title: Creativity Is Giving a Horse Its Head
date: 2023-02-27T21:00:44.226Z
draft: false
featured: false
tags:
  - Work
image:
  filename: creativity-giving-a-horse-its-head.png
  focal_point: center
  preview_only: false
  caption: Illustration by Midjourney
  alt_text: illustration of a cowboy on horseback approaching the top of a steep canyon
---
I read a bunch of Louis L'amour novels in college. [Louis](https://www.louislamour.com/aboutlouis/biography.htm) is a legend. He worked in mines and lumber camps across the American West in the early 1900’s. He was a professional boxer, handled elephants for a circus in Asia, and commanded a company of gas tankers on the Western Front during WWII. His life defines The Greatest Generation as we like to remember it. Hardworking, patriotic, stoic, and determined. 

He was also a prolific writer. By the time of his death in 1988, he was a bestselling author with over 100 published works, and he was the proud recipient of both the Congressional Gold Medal and Presidential Medal of Freedom.

Most of his works were Western Novels, which he preferred to call *Frontier Stories*. The protagonists in his stories are typically [chaotic good](https://en.wikipedia.org/wiki/Alignment_(Dungeons_%26_Dragons)), so they’re often on the run from ghosts of their pasts.

In [High Lonesome](http://www.louislamour.com/novels/highlonesomeLegacy.htm), Considine finds himself running from a slew of these ghosts when he hits a can’t go under it, can’t go around it, gotta go through it situation. He pulls up on a deep canyon with steep banks. With a trigger happy posse hot on his tail, he has no choice but to chance the descent. He picks a path, guides his horse to the edge, and gives his horse its head.

I had to pause when I read this. I didn’t know what it meant to give a horse its head. I had a hunch. But I didn’t know. 

Keep reading and I’ll explain the concept and how it captures the essence of creativity. But first let’s consider how modern work culture kills creativity.  

## The age of overload

Peter Drucker coined the term knowledge worker 63 years ago to capture a fundamental shift in the way we work. Thanks to technology, work was less manual and less tactile than ever before. Knowledge, critical thinking, and creativity had become paramount, and productivity looked fundamentally different. 

Knowledge work can’t be engineered for efficiency like the manual processes of earlier industrialization. It’s mushy. It comes with objectives and goals rather than instructions and processes. It involves trusting workers to figure out how to best accomplish these goals instead of applying rigid systems to control output. So, quantifying knowledge worker productivity, especially on a granular level, is difficult.

Here’s the crux of the problem: Knowledge work depends heavily on creativity, but creativity is too abstract to measure. Productivity and creativity cannot survive without one another, but they exist with innate tension. Over-optimize for productivity and creativity will suffer. Over-optimize for creativity and productivity will suffer. Go too far in either direction, and **both** suffer.

With knowledge work, you have goals and objectives on one end, and positive results (hopefully) on the other, but what happens in the middle is a black box. Some companies will have a better handle on what happens inside the black box than others, but even the most sophisticated companies will have a high degree of unknown. 

![](knowledgeworkflow.png)

**That black box is fertile ground for creativity.** But there’s also a creativity killing factor at play–uncertainty. [We’re naturally averse to it](https://www.sciencedirect.com/science/article/pii/S0887618516300469), so we try to make the mushy concrete. We far over optimize for productivity because it feels safer to have something to grab hold of that we can measure. But what do we measure? What can we extract from the black box to relieve our uncertainty?

The easy answer is tasks. Checking off tasks feels productive and concrete. **But whether each task directly and substantially drives progress towards positive results is often overlooked.** Doing more feels safer than doing less. The means become the end, and things devolve into a creativity killing task circus where [getting things done](https://www.amazon.com/Getting-Things-Done-Stress-Free-Productivity/dp/0143126563/ref=asc_df_0143126563/) is paramount and workers are overloaded. 

### Advanced communication 

We evolved communication to meet us in the frenzy. But our advanced communication methods only serve to [supercharge the overload](https://www.newyorker.com/culture/cultural-comment/slack-is-the-right-tool-for-the-wrong-way-to-work). In [Writing That Works](https://www.amazon.com/Writing-Works-Communicate-Effectively-Business/dp/0060956437), Kenneth Roman and Joel Raphaelson highlight the dangers of email, “Warning: E-mail can be addictive and create problems of its own. Its emphasis on speed conflicts with matters that deserve thought and reflection.” This was published in 2000. I’ll let you decide whether we’re better or worse off now that we’ve moved beyond email into the more instantaneous world of Slack et al.

[Under pressure](https://open.spotify.com/track/1mSClObliRtgPVT399COQH?si=gU2GOaXwTLylQFzB1H3Y2A&context=spotify%3Asearch%3Aunder%2Bpres) to check off more tasks, we scramble to get crap off of our own plates as fast as possible. We flood each other with an endless deluge of distractions because our individual productivity is highly dependent on everyone else. So, if you’re blocking me from checking off a task, beware. I don’t know or really care what you’re working on at the moment. I just need *you* to prioritize whatever is blocking *me*… ping, ping, ping.

Our quest to meet objectives devolves into a game of task whack-a-mole or a maniacal [crank](https://www.43folders.com/2011/04/22/cranking) depending on how we’re wired up. 

## & hustle culture

We created hustle culture to cope with the overload. It’s like we needed to convince ourselves we actually love it. With hustle culture intact, the overload isn’t negative, it’s a badge of honor. Living for the Hustle is the pinnacle of existence, or so we tell ourselves. There’s a load of anxiety lurking beneath the glossy surface, but we’re [fine](https://www.dictionary.com/e/memes/this-is-fine/)

We worship the people who appear to have mastered the hustle. [Tim Ferris](https://tim.blog/) built a career dissecting how high achievers achieved their heights. Gary Vee has 4 million subscribers on Youtube because I guess we think it’s awesome to [hustle our faces off 15 hours a day](https://youtu.be/PIJElPStJpg?t=151). We’ve even co-opted meditation as a tool to fuel hustle. I mean, if you told me this article, [How to 'Micro-Dose' Meditation for Maximum Productivity](https://www.inc.com/nate-klemp/think-you-dont-have-time-to-meditate-science-shows-that-even-a-few-minutes-can-boost-your-productivity.html) was a satirical piece from The Onion I’d believe you. But it’s not. It’s a real article from Inc. Magazines [Most Productive Entrepreneurs](https://www.inc.com/extreme-productivity) (I told you we worship them) section. Here’s how it ends: “The best thing about this practice is that it's portable and ultra-efficient.” Are you kidding me? I don’t think meditation was ever intended to be portable and ultra efficient.  

We’ve pushed so far into productivity mania that our hustle amounts to more toil than progress. In fact, [productivity is in decline](https://www.npr.org/2022/10/07/1126967875/quiet-quitting-productivity-workers-ennui-working-jobs#:~:text=The%20result%3A%20This%20year%2C%20productivity,the%20number%20back%20in%201948.) in America. That’s in spite of hustle culture and our intense focus on productivity. The mania is especially destructive for creative work because such an intense focus on productivity triggers creative claustrophobia and

Creativity requires s p a c e. 

## Give the horse its head

You can’t force the creative process into an assembly line. It should be approached systematically, but it can’t be put on railroad tracks, it needs a highway. This is what it means to give a horse its head. 

You *loosen* the reins and allow the horse to take over. You stop trying to *control* it so much. It requires trust. But it frees the horse to do what it naturally does best. In High Lonesome, Considine loosened the reins because he knew his horse could intuitively find a safer path down the canyon than he could. 

But he didn’t let go of the reins altogether. He held on to keep his horse moving in the right direction.

### Let your mind wander, just not out of bounds

This is how creativity works. When you stop trying to force it. When you [let your mind wander](https://www.verygoodcopy.com/verygoodcopy-blogs-9/spurring-creativity), in a direction, instead of fighting it, you allow it to connect things. To be creative. Sometimes this looks like wasted time.

But **creativity is not lazy**. In fact, it often follows a rigorous, if loose, process. Graham Wallas formalized a four step process in his 1926 book [The Art of Thought](https://www.amazon.com/Art-Thought-Graham-Wallas/dp/1910146056). It’s helpful, but at the end of the day the most important thing about a creative process is that it works for you. I like to keep things simple. Here’s how I think about it:\
\
There is input and there is output. Inbewteen there is giving a horse its head.

Intense **input** is key to unlocking your creative unconscious. Input can come from everywhere. In [The Adweek Copywriting Handbook](https://www.amazon.com/Adweek-Copywriting-Handbook-Advertising-Copywriters/dp/0470051248), Joseph Sugarman helpfully breaks it into two categories:

* **General knowledge** – This comes from all of the experiences, relationships, and knowledge you’ve amassed over time. It’s fueled by travel, reading, movies, music, hobbies, almost everything. 
* **Specific knowledge** – Specific knowledge comes from rigorous research on the project at hand. It’s everything required to become an expert on whatever you’re trying to create. It could come from reading, interviewing, or for more artistic endeavors, deep introspection.

Spending time to fill your hopper with quality input and giving your mind some gentle guidance will enable you to loosen the reins, trusting it to work hard and work in the right direction. So input, input, input. Then stop. **Give the horse its head**. 

Anne Lamott captures this wonderfully in [Bird by Bird](https://www.amazon.com/Bird-Some-Instructions-Writing-Life/dp/0385480016), “You have to relax, and [woolgather](https://www.dictionary.com/browse/woolgathering), and get rid of the critics, and sit there in some sort of self-hypnosis”

Then you listen. You let your unconscious work, and you listen. And when it begins talking, you capture the ideas.

And so **output** begins. You’ll find the right way to capture things. I like to write everything down or draw on premium white paper. Like 32 lb. copy paper. The kind that feels smooth and substantial. The pages with no lines ease any sense of claustrophobia for me, and the weight feels satisfying. It might be weird, but it oils my creative gears. You’ll find that some idiosyncratic methods aid your process too. 

Once I’m satisfied that my subconscious is finished talking, I take everything I’ve captured and begin to organize it into something useful. Sometimes this means spreading my no longer blank copy papers all over the floor and grouping them together. Or maybe drawing lines between words on the paper. Once I’ve processed the output and organized it, only then do I go on to the execution. Only then do I let myself worry about getting things done and shift my focus from creativity to productivity.

## Stop crying and just keep hustling

I often find myself paralyzed in this age of overload. Bogged down by a never ending list of tasks, fighting for an ounce of focus, and frustrated that I can’t seem to squeeze anything out of my brain. Sometimes I even go as far as wondering why I can’t seem to be as productive as everyone around me and wishing I were wired differently. 

I think we’d be wise, as a working society, to resist the urge to throw more tasks at the wall in an effort to handle the uncertainty intrinsic to the black box between goals and results. We could instead use that energy to understand what’s working at a higher level, and do more of that. Until then, I’m learning to loosen the reins anyway. No matter what those around me are doing, I can trust that if I carve out just enough space for creativity, the results of our work will improve.

If you find yourself looking over the edge of the canyon, paralyzed, it’s up to you. Give the horse its head. Or, if you’d rather lean into the empty promise of hustle culture, you can [stop crying and just keep hustling](https://youtu.be/PIJElPStJpg?t=261). But my hunch is we stand a better chance of crossing the canyon alive if we loosen the reins.