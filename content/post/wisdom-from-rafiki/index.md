---
title: Wisdom from Rafiki
date: 2021-12-22T04:27:32.912Z
draft: false
featured: false
tags:
  - faith
image:
  filename: featured
  focal_point: Smart
  preview_only: false
---
Years ago I received a word of encouragement that I’ve gone back to time and time again. It was during my sophomore year of College at Clemson. I was treading through a rough season and feeling the weight of discouragement when a friend reminded me of this [scene](https://youtu.be/tmWKvvXzlyg) from The Lion King. 

This piece of Simba’s story is always a refreshing reminder when my heart is weary.  It resonates with me because I’m someone who struggles with insecurity. I often feel fearful and inept - as I imagine Simba does here. It takes persistent Rafiki beating him over the head with the truth to awaken the courage that [slumbers](https://youtu.be/JIMt-omB8Ck) inside of him.

 Sometimes I need to be beat over the head with the truth too.  

They may not leave on knot on your skull, but these five minutes, backed by a score from the legendary [Hans Zimmer](https://en.wikipedia.org/wiki/Hans_Zimmer), pack some heavy duty truths:

* We are children of the King
* The King is alive
* His power lives in us

## A Little Confused

The scene begins with Simba disoriented and discouraged. He’s doubting his past, his purpose, and the promises of his father. I bet we’ve all felt this way at one time or another, doubting if God is really with us or if He even cares. Wondering, like Simba, who we are.

## You’re Mufasa’s Boy

This is when the crazy baboon Rafiki dances back into the story (bear with me here as I dare to compare Rafiki to the [Holy Spirit](https://www.biblegateway.com/passage/?search=John%2014%3A26&version=ESV)). He begins riddling Simba’s troubled mind,  initially causing more confusion than clarity. But he’s beginning to redeem Simba from his despair. You see a glimmer of hope in the young lions eye when Rafiki reminds him he is Mufasa’s boy - Mufasa the King. 

[Romans 8:16](https://www.biblegateway.com/passage/?search=romans+8%3A16&version=ESV) tells us that “the Spirit himself testifies with our spirit that we are God’s children.” The next verse explains that if we are God’s children, then we are heirs to His throne. This statement has infinite weight, and it bears repeating. We are God’s children, [holy and dearly loved](https://www.biblegateway.com/passage/?search=Colossians%203%3A12&version=NIV). We are heirs to his heavenly throne. 

But it doesn’t mean much to be the child of a King who holds no power. Simba believed his Father was dead. That’s why he fled into exile and embraced [hakuna matata](https://www.youtube.com/watch?v=BAoCYwefq1A). And it’s why the next part of the story is so important. 

## He’s Alive, and I’ll Show Him to You

After grabbing his attention by reminding Simba of his nobility, Rafiki boldly claims that Mufasa is alive. Ashamed, Simba corrects him, believing the King is dead along with all the hope and power he represented. Simba’s remorse is palpable, but Rafiki is unhindered. He continues in his quest to show the young lion the truth.  His  joyous confidence challenging Simba’s despair.

Often I need to be reminded that God is alive, “I am the Living One; I was dead, and now look, I am alive for ever and ever! And I hold the keys of death and Hades” ([Revelation 1:18](https://www.biblegateway.com/passage/?search=Revelation%201:17-19&version=NIV)). This truth is imperative to his power in my life. It's just not always easy to believe when the heaviness of life bears down.  

This is where Simba finds himself. Rafiki has confronted him with a powerful idea that changes everything if true, but he needs more convincing. Rafiki understands this, so he offers to introduce the child to his father in a new, deeply personal manner. 

## Don’t Dawdle 

The baboon dances away just as quickly as he came, urging Simba to follow and assuring him that he knows the way. He dives into a thicket, leaving Simba no choice but to run after him or get left behind.

This is often how the Holy Spirit works. Elusively running ahead, often through frightening and difficult places, begging us to trust and to follow. For He knows where he is going, and He knows it is good. 

## He Lives in You

Simba finally catches up to Rafiki after chasing him through the darkness, at times hesitantly. The baboon leads him down to a pool where he urges him to look at his reflection. At first Simba’s shame and doubt cloud his view. All he can see is the disappointment he’s become so accustomed to, but Rafiki knows better. The wise old sage prompts Simba to look harder. Simba does, and he begins to see. “He lives in you,” Rafiki declares. 

This is just as God the father sees his own son, Jesus, reflected in us instead of all the sin, shame, and failure that we often dwell on and see in ourselves.

Perhaps we should consider that Jesus has made us clean and that he commands us, “Do not call anything impure that God has made clean.” ([Acts 10:15](https://www.biblegateway.com/passage/?search=acts+10%3A15&version=NIV)) How would our days change if we lived in the confidence that Christ has made us pure and grew to understand just how precious we are in his sight? 

What humbling power would we know if we walked in the truth that the Spirit of God is always with us? “And I will ask the Father, and he will give you another advocate to help you and be with you forever— the Spirit of truth. The world cannot accept him, because it neither sees him nor knows him. But you know him, for he lives with you and will be in you.” ([John 14:16-18](https://www.biblegateway.com/passage/?search=John+14%3A16-18&version=NIV)) 

As Simba starts to recognize his father in himself, his  heart begins to change, the chains of guilt and shame loosen, but it’s what happens next that sets him free. 

## I’m Going Back

The mighty king comes down to meet his wayward son. In the midst of Simba’s confusion, he brings clarity. He reminds Simba “you are more than what you have become,” and charges him to take his place in the [circle of life](https://www.youtube.com/watch?v=GibiNy4d4gc). Still fearful, Simba asks “how,” but Mufasa doesn’t entertain the final effort to dodge responsibility. Instead, the departed King calls courage out of his son with a powerful reminder “remember who you are, you are my son and the one true king” and he departs.\
\
Simba longs for Mufasa to stay but quickly accepts the departure and sets his mind on what lies ahead. He is changed. The encounter with his father broke the chains that bound him for so long. Finally he has found hope and courage again. He knows his father is alive, he believes that the king’s power lives inside of him, and he resolves to go back home. Rafiki is left celebrating, and Hans Zimmer’s score takes a triumphal turn.

[Ephesians 2:10](https://www.biblegateway.com/passage/?search=ephesians+2%3A10&version=NIV) tells us that "we are God's handiwork, created in Christ Jesus to do good works." So, let us remember God’s promises, shake of our guilty fears, and [strain toward what is ahead](https://www.biblegateway.com/passage/?search=Philippians%203%3A13&version=NIV). For we are God’s Children. God is alive, and his power lives in us.