---
title: Mountain Biking Applied to Startup Life
date: 2021-12-22T04:30:58.417Z
draft: false
featured: false
tags:
  - work
image:
  filename: ""
  focal_point: Smart
  preview_only: false
---
The idea for this post came to me deep in the woods of [Pisgah National Forest](https://en.wikipedia.org/wiki/Pisgah_National_Forest). It was a hot and humid July day in 2020. I was burnt out in my job at the time and considering a change. I needed space to reflect on what next steps might look like for me professionally, so I took a few days off and sought refuge in the mountains. I spent the days hiking and mountain biking. I spent the nights reading and [writing my way out](https://youtu.be/7ZfzuJ8oVpE) of a funk.\
\
Pedaling up Spencer Branch Trail one afternoon to earn a rowdy decent on Trace Ridge, something clicked. The air was thick and the forest dense, but my mind was clear. My brain was firing on all cylinders. I realized many of the lessons I've learned from mountain biking apply to my professional life. I stopped a few times to stuff my thoughts into my phone, and I kept pedaling.

This was six months before I naively dove [headlong](https://youtu.be/zhyaAPsT1LU) into startup life. The lessons I get into below seemed applicable at the time. Little did I know how much more pertinent they'd become after joining [RudderStack](https://rudderstack.com/) during seed stage.

### Pick good lines

There's always more than one way to achieve a desired outcome, but how you get there makes a difference. Pros can quickly assess the landscape, visualize success, make a decision, and roll down the optimal line. They also know when to get off their bikes and do a little research before blasting down a gnarly line. Sometimes they do things methodically, and sometimes they just trust the instincts and competencies they’ve developed. 

### It's okay if you don't pick the perfect line

Nine times out of ten you'll get where you're going even if you didn’t pick the perfect line. Usually the consequences of failing aren’t nearly as dire as you think they'll be. Besides, **failure is still experience,** and if if you want to [level up](https://youtu.be/3suGfhnT2Sg), you're going to need a lot of XP. 

### There are no perfect lines

None. There's always room to do better, and there are people who will find faults with even your best work. The requirement for action can't be perfection. It's better to pick a good line, drop in, and start making [progress](https://medium.com/swlh/why-your-progress-is-more-important-than-perfection-1dae5e7482b7). Progress is something you should be proud of even if it isn't perfect.

### Velocity covers a multitude of mistakes

If [love covers a multitude of sins](https://www.biblegateway.com/passage/?search=1+Peter+4%3A8&version=ESV), when it comes to mountain biking and startups, velocity covers a multitude of mistakes. Stop thinking so hard about the challenges in your way. You won’t overcome them by obsessing over them. Make a good assessment, make a decision, and keep freaking moving. Momentum is your best friend, and when you lose it it's hard to get back.

### [Hold on loosely](https://youtu.be/g3nn6WfFQ7o)

If you cling too tightly, you're gonna lose control.

### Look at a map first (read the instructions)

I hate maps, and I don't like to read instructions. I've always taken more of a brute force approach, but I'm learning how foolhardy this is. If you don't know where you're going, someone has probably been there before, and there's a good chance they'd be happy to show you the way. They may have even already documented it. Look at the map first. Learn how to use google to find answers. Watch YouTube videos. Reach out to someone who's been there before. And once you've gathered enough information, go freaking do it. Don't expect anyone to hold your hand, and don't doubt yourself.

### Your bike will squeak - learn to fix what you can and know when to ask for help

As a rule of thumb, you should always try to figure things out for yourself first. Critical thinking is a muscle that you can build. Learning to think and work independently means you can get more done and you can help others do more. But we all have limits. Trying to fix something way outside of your wheelhouse is a waste of precious time. Don't let pride or stubbornness keep you stuck. Learn when and how to tactfully ask for help, but remember, asking for help always goes over better when people can tell you're busting your ass.

### Things get worse if you don’t address them

That noisy drivetrain isn't going away on its own, and weak brakes won't fix themselves. It's insane the amount of things we are acutely [aware of yet choose to ignore](https://www.ted.com/talks/michele_wucker_why_we_ignore_obvious_problems_and_how_to_act_on_them/transcript?language=en). This is true in relationships, systems, and processes. Yes, it's important to prioritize and tackle problems strategically, you can't fix everything at once, but that's a post for another day. The point I'm trying to make here is simple - stop avoiding your problems.

### Know when you need to rest

Sometimes a quick breath, a long weekend, or a nap is all you need to get back to peak performance. But if you're pushing hard, at some point you'll hit place a of higher exertion that requires real recovery. Having the awareness to take a breather when you need to can keep you going. Having the wisdom and humility to get out of the saddle and take time for heavy duty recovery will save you from crippling burnout. 

### Do bigger stuff, but be smart about it

Progress is important. You can get better. You were made to get better. But you can't go from level 5 to level 75 overnight. Learn the fundamentals of whatever you're working on, get your reps in, stay ambitious, but be patient with yourself. When you look back in a few months, you'll be surprised how much progress you've made. And take a risk every now and then - [there's great deal more in you than you guess](https://www.goodreads.com/quotes/9669872-if-i-say-he-is-a-burglar-a-burglar-he).

### Stop and take it in

You're probably moving way too fast to see some pretty amazing things around you. [Ferris Bueller](https://www.youtube.com/watch?v=vsYBtfQ3QDo) wasn't kidding when he said "life move's pretty fast. If you don't stop and look around every once in a while, you could miss it". It's good to pause and be mindful of things you've accomplished and the people who have helped you along the way. 

### Be kind

You didn't get where you are by yourself, and no one else did either. Making progress is difficult. Keep this in mind and be gracious to everyone else out there with the hammer down. Recognize when people are struggling, and push yourself to help others even when it costs you.

### Share your local knowledge

This is an easy way to make a big impact. New places and new challenges get a lot smaller and a lot more manageable when people who are already familiar with them take the time to walk you through them. Seek out people who can do this for you, and do it for others whenever you can.

### Surround yourself with people who are better than you

It can be intimidating at first, but you'll improve almost automatically when you ride with people who are better than you. Some people will be assholes. Find the one's who aren't and stick close. And never become one of the assholes.

### Just. Keep. Pedaling.

At the end of the day, no matter how hard things get or how impossible they seem, the most important thing is to [keep moving](https://youtu.be/2qocmVDu3BE) in the right direction. It doesn't matter how things are going at the moment. Don't freaking stop. Conditions might not improve, but you can get better, and that's enough. Pedal forward.