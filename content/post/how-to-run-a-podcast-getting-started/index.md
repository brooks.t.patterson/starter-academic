---
title: "How To Run a Podcast: Getting Started"
date: 2023-04-05T11:13:42.548Z
draft: false
featured: false
tags:
  - Work
image:
  filename: pedalforwardsocial.jpg
  focal_point: Smart
  preview_only: false
---
[The Data Stack Show](https://datastackshow.com/) may have been born out of madness. COVID lockdowns drove us all to do some crazy things. [Kostas](https://twitter.com/KostasPardalis) decided to start a podcast. With no prior experience and no expertise, he dove right in. Crazy or not, I’m glad he did, as the show has taken off, and I’m fortunate enough to be a part of it today.\
\
The [first episode](https://datastackshow.com/podcast/01-tools-that-optimize-dev-workflows-with-alex-dovenmuehle-of-mattermost/) dropped on August 12, 2020. After four episodes solo, Kostas decided he needed a wingman and convinced [Eric](https://www.linkedin.com/in/ericdodds/) to be his co-host. Together, they willed the show along for 20 episodes, but they were losing steam. 

I jumped in at that point to help produce the show. My job is to make it easy for them to be the show hosts and not have to worry about much else. I make sure we deliver an episode every single week and that the show continues to grow. 

As of writing, we’ve put out a new episode every week for over 2-½ years. We’ve released 132 episodes featuring guests from Netflix, Stripe, Facebook, and many of Silicon Valley’s hottest data startups. We’ve carved out a strong following among data enthusiasts – over 2,000 people listen every month.

Consistently delivering a new episode week in and week out is hard, but consistency is vital to the success of the podcast. We’ve learned many lessons, mostly the hard way, making it happen over the last few years. I’ll share these lessons below.

If you run a podcast, host a podcast, or want to start a podcast, these lessons will help you avoid some pain along your journey. Hopefully they’ll help you have some fun and find success. In this post I’ll cover the two most important aspects of getting started – administration and content. Later, I’ll write on guest experience and show growth. 

## Get to twenty

If you want to start a podcast. Do it. But take a page out of the endurance athletes playbook and commit to creating 20 episodes like a training program. Don’t let yourself quit. Because once you get to 20 episodes, you’ll realize you can keep going. 

But getting started and getting to 20 is going to hurt, so you’re going to want to make it as easy on yourself as possible. That’s where administration comes in.

## Administration

Administration is a grim reaper. It kills many shows on the vine, and it almost got The Data Stack Show. Because even if you can get top shelf guests to agree to come on your show, and you have all the other makings of a surefire winner, you still have to do the legwork to make it happen. If you want to make it, you need to remove as much friction as possible from the process of scheduling, recording, and shipping a show.

Neither Eric, Kostas, nor I are particularly administratively gifted. We learned the hard way that brute force admin would eventually kill the show. It required too much of our time and energy. We needed to automate as much as possible. Here’s what we did:

### Time blocks and scheduling tools

We’ve all been there, swapping piles of emails to find a time to meet. Trying to coordinate calendars is a waste of time. For The Data Stack Show, we’re coordinating amongst two busy hosts and sometimes multiple guests whose calendars we don’t have access to, so it got untenable quickly. We needed to effectively eliminate this overhead, and we did. It took two steps – implementing a scheduling tool and establishing recording blocks on Eric & Kostas calendars.\
\
**First, we got a scheduling tool** so guests could view availability and book a recording slot in real-time. No more cross checking calendars for availability and offering times over email that may or may not even work for the guest. We picked [Savvycal](https://savvycal.com/) – It’s a bit more sophisticated than Calendly, lets us check for conflicts against multiple calendars, easily send invites for bookings to everyone on the team, and even build in some cool automations for guest comms (more on comms below).

**Second, we reserved two time blocks a week for recordings** on Eric and Kostas calendar’s. This way recordings are consistent, not based on random availability. Reserving time blocks further eliminated mental overhead for the whole team, and it established a valuable routine. If you’re going to be a creator, you need routine. In [Steal Like an Artist](https://austinkleon.com/steal/), Austin Kleon says “Establishing and keeping a routine can be even more important than having a lot of time \[to create].” Here’s why it’s important to The Data Stack Show.\
\
With time set aside, Eric and Kostas don’t have to worry about keeping track of when they’re recording week in week out. They just know that every Tuesday and Thursday from 4-5:30 it’s showtime. This allows them to fully rest from their host duties in between recordings. The consistency also cultivates two surprisingly related qualities: discipline and joy. Routine naturally establishes a helpful discipline, and the baked in anticipation gives them something to look forward to. 

### Zoom

We record the show on Zoom. Everyone knows how to use it and is generally comfortable with how it works. Recording a podcast can be stressful enough for our guests, no need to add using a new tool for the first time to the plate. Even if it’s a seemingly trivial thing, we’ve found that comfortable guests make for great shows, and every little thing we can do to make them comfortable is worth it. It makes our lives easier too, and saving the files locally instead of in the cloud provides great audio quality.

### Boilerplate comms

I used to waste a lot of time writing emails from scratch for every guest. It felt like a nice authentic touch. But the truth is people just want cordial and efficient communication. So, I made boilerplates for all of our standard communications. They’re nice. They’re clear. They’re short. They do their job. We have five boilerplates that cover most of our comms:  

* Invitations
* Scheduling + recording details
* Recording follow-up & thanks 
* Launch day reminder (week of)
* Launch day reminder (day of)

I do often add personal notes to these, but the boilerplate takes care of the heavy lifting. Standardizing communications also helps ensure a consistent guest experience, which gets increasingly important as your show grows.

### Don’t prepare

We don’t prepare for the show ahead of time. I’m serious. This works for us. In fact, it really has to work for us. With two hosts who work full time in demanding jobs and guests who are often even busier, laborious show prep just isn’t in the cards. We tried it. I used to send a Google doc out to Eric, Kostas, and the show guest ahead of time with some questions designed to identify talking points. Usually all I got back was a few fragmented ideas and a pretty empty Google Doc. It wasn’t working, so we stopped doing it. 

But we didn’t really stop preparing altogether, we came up with a way to prep that actually works for us and for our guests. We now book 1.5 hours for every recording instead of 1 hour. We use the additional 30 minutes to prepare with our guests in real-time just before we record. 

Our show is informal and conversational, so this works. In fact, the show got better when we stopped trying to force too much structure. Now we bring guests into the process and work with them to identify 3-4 higher level topics for their episode. With a direction set, but not dictated, Eric and Kostas are able to guide the conversation through these topics digging into the most interesting specifics organically along the way. Guests and listeners alike have given us great feedback for this style.\
\
I realize this is not the optimal format for every show, but it’s done wonders for The Data Stack Show. I’d argue that it’s a great way to get started if you’re launching a new show. It will help you make it to 20, and it will help you determine if/how you might actually want to change the format as you get more experience. Get it going, and you’ll develop conviction about the optimal format as you go. 

### Outsource audio/video and creative

Bogging yourself down trying to do something you’re not experienced with and don’t have time for, like editing audio or creating graphics, will torch your chances of making it to 20. Don’t do it. If you think it would be interesting to learn audio editing, save it for hobbying. You won’t be efficient, and you’ll put your show at risk. Even if you are an expert, I’d caution you against trying to handle this stuff yourself. Taking on the show will add enough to your plate as is. 

We have an [agency](https://www.trustheard.com/) that handles our website, audio and video editing, publishing, and promotional graphics for us. They rock. They make it easy for us to focus on the show, and they’re cost effective. We wouldn’t still be rolling if it weren’t for them. You’ll want to find an agency that can provide this kind of lift for you, too. 

### Get more help

Even if you manage to automate the lions share of your process, there’s still much that must happen to take a show from scheduled to published. As your show grows, if you want to maintain velocity and begin experimenting with different tactics to expand, enlisting more help can provide the extra energy you’ll need.

Eventually, we recruited Ali, our resident project management wizard and undercover superhero, to help us improve our processes and make sure we don’t drop any balls. Her support has proven invaluable as things with both The Data Stack Show and our day jobs has accelerated dramatically since we started the show. 

There’s a caveat here. The Data Stack Show benefits from its sponsoring champion, [RudderStack](http://rudderstack.com/). As a company sponsored show, we’re able to tap resources that aren’t available to everyone, but if you have access to help or can scale in a manner that affords you to, get more help. 

### Administration actionables:

* Block a consistent time for your recordings each week and stick to these times
* Use a scheduling tool (we use [Savvycal](https://savvycal.com/)) to book your recordings
* Boilerplate your standard communications
* Don’t overburden yourself or your guests with preparation
* Outsource the technical stuff to a dedicated team
* When you start dropping balls, get more help

## Making great content

The formula for great podcast content isn’t rocket science: bring on an interesting guest and ask them interesting questions. It really is \[almost] that simple. Let’s break down these two main ingredients, the host and the guest. 

### Hosts 

The foundation of good content is a host who’s creating every episode because deep down they feel compelled to. The show’s subject matter has got to be the stuff that lights them up.\
\
Great art – and it is helpful to think of your podcast as art – isn’t created first for the audience, it’s created by an artist with something inside of them they feel compelled to share. 

When things are created with this kind of enthusiasm, they resonate with others. Anne Lammot says “If something inside you is real, we will probably find it interesting, and it will probably be universal.” So, creating great podcast content has to start with creating something real. Something that lights up the creators. Something they can talk about for hours (because that is exactly what they’ll be doing) and not grow weary.\
\
There’s also a consideration on longevity here. Getting to 20 is hard in and of itself, but I’m guessing your hope is to continue well past episode 20. Creating content on a subject that the host isn’t all in on, will end in burn out, so save yourself the trouble. Don’t start the show if it’s not about something the team would happily stay up until 2AM reading about.\
\
Now, I’m not saying don’t think about the audience. You should work to understand your audience and tailor the show to them. But at a foundational level, you can’t start a show based on what the “audience” wants to hear. You’ve got to base it on what you have to say – what you love to nerd out on. This will align with a group of people. Once you get that figured out, you get the joy of learning more about them and making the show awesome for them. 

As far as skill and polish go, it is important for a host to become a great interviewer, but this can take time. The good news is, curiosity and a dose of humility covers inexperience, and listeners are forgiving. In fact, improving over time will actually endear you to your audience. Starting a podcast is tough, and hosting a show involves a great deal of vulnerability, so hosts must be gracious with themselves along the way. Stick to the program, get to 20. Once you get there, the whole team will start hitting a groove, and the hosts will start settling into the role. 

### Guests 

Everyone has a story to tell. If your show is an interview based show, the point should be to help your guests tell their most interesting stories. If you find guests who are as passionate about the subject as the hosts are, you’ll make great shows. Don’t worry too much about finding dynamic personalities. That’s a bonus, sure, but it should be secondary to subject knowledge and interest. Everyone lights up when they talk about things they love. 

I’ve seen it on the show over and over and over. A guest comes on and seems quite reserved, perhaps even inarticulate, but totally transforms when Eric and Kostas begin asking them questions about battles they’ve won or problems they’re currently solving. If you make polish a criteria for your guests, you’ll miss out on some of the best conversations. Besides, helping people tell their most interesting stories is incredibly rewarding for the show host, which is another win for longevity.  

### Show voice 

You’ll find the voice of your show as you go. It will take some time for you to develop conviction around it, but that’s okay. Just start publishing shows. Pause to thoughtfully consider how the show’s personality is emerging every so often. Think about what’s working, what feels natural and comfortable, and do more of that. It will look different for everyone, but here’s what we’ve learned.

Your show should be flavored by the personality of the hosts. Podcasting is a personal form of media. Think about the shows you follow. You probably feel a high level of affinity towards the hosts, even though you’ve never met them. So, your show should help people get to know the host.

A practical way to think about this is to consider how to make the show fun for the hosts. If their personalities, idiosyncrasies, and preferences are reflected in everything from the show theme music to the show format, the hosts will have fun making the show. They’ll be proud of it, and it will develop a personality of its own that reflects theirs. Plus, hosts are more comfortable recording shows when they’re having fun, and this will go a long way in making your content even better.

### Content actionables:

* Make the show about something you care about
* Talk with guests who care about it as much as or more than you do
* Help them tell their most interesting stories
* Don’t worry about perfection – just keep getting better every week

## Ready, set, go

Running a podcast isn’t easy, but if you can make it to 20 you can make it to 130, and if you do the things outlined above, you can make it to 20. Remove as much friction from the process as possible. Make the show about something the hosts care about, bring on guests who care about it as much as they do, and your content will be good. Help your guests tell their best stories, and don’t worry too much about polish – you’ll get better and better as you go. 

If you start with this foundation, you’re already ahead of the game. Next I’ll write about guest experience, experimenting, and growth, so stay tuned.