---
title: Learning

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---

[Writing to learn](https://www.jowr.org/articles/vol7_3/JoWR_2016_vol7_nr3_Klein_Boscolo.pdf). 
