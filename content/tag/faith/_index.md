---
title: Faith

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---

Processing the tension between truth & grace.